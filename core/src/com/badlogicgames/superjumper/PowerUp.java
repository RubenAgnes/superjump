package com.badlogicgames.superjumper;

public class PowerUp extends GameObject {
    public static final float PU_WIDTH = 0.5f;
    public static final float PU_HEIGHT = 0.5f;

    public int type;
    float stateTime;

    public PowerUp (float x, float y) {
        super(x, y, PU_WIDTH, PU_HEIGHT);
        stateTime = 0;
        type = 0;
    }

    public void update (float deltaTime) {
        stateTime += deltaTime;
    }
}