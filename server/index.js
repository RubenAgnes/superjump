var express = require('express');

var app = express();

var highscore = "10";

app.get('/', function(req, res){
	console.log(highscore);
	res.write(highscore);
});

app.put('/:score', function(req, res){
if(req.params.score > highscore){
	highscore = req.params.score;
}
console.log(highscore);
res.end();
});

app.listen(3000, function () {
  console.log('App listening on port 3000!')
})